// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alive": MessageLookupByLibrary.simpleMessage("Alive"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "create": MessageLookupByLibrary.simpleMessage("create"),
        "dead": MessageLookupByLibrary.simpleMessage("Dead"),
        "english": MessageLookupByLibrary.simpleMessage("english"),
        "enter": MessageLookupByLibrary.simpleMessage("Enter"),
        "findPersonage": MessageLookupByLibrary.simpleMessage("Find Personage"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "loginLenWarning": MessageLookupByLibrary.simpleMessage(
            "Login must have minimum 3 signs"),
        "noAccYetQuestion":
            MessageLookupByLibrary.simpleMessage("Do You hav\'nt accaunt yet?"),
        "pass": MessageLookupByLibrary.simpleMessage("Pass"),
        "passLenWarning": MessageLookupByLibrary.simpleMessage(
            "Pass must have minimum 8 signs"),
        "personages": MessageLookupByLibrary.simpleMessage("Personages"),
        "russian": MessageLookupByLibrary.simpleMessage("russian"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "totalPersonages":
            MessageLookupByLibrary.simpleMessage("Total Personages"),
        "tryAgain": MessageLookupByLibrary.simpleMessage("Please, try again"),
        "unknown": MessageLookupByLibrary.simpleMessage("Unknown")
      };
}
