// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru_RU locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru_RU';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alive": MessageLookupByLibrary.simpleMessage("Живой"),
        "close": MessageLookupByLibrary.simpleMessage("Закрыть"),
        "create": MessageLookupByLibrary.simpleMessage("создайте"),
        "dead": MessageLookupByLibrary.simpleMessage("Мертвый"),
        "english": MessageLookupByLibrary.simpleMessage("английский"),
        "enter": MessageLookupByLibrary.simpleMessage("Вход"),
        "findPersonage":
            MessageLookupByLibrary.simpleMessage("Найти персонажа"),
        "login": MessageLookupByLibrary.simpleMessage("Логин"),
        "loginLenWarning": MessageLookupByLibrary.simpleMessage(
            "Логин должен содержать не мене 3 символов"),
        "noAccYetQuestion":
            MessageLookupByLibrary.simpleMessage("У Вас еще нет аккаунта?"),
        "pass": MessageLookupByLibrary.simpleMessage("Пароль"),
        "passLenWarning": MessageLookupByLibrary.simpleMessage(
            "Пароль должен содержать не мене 8 символов"),
        "personages": MessageLookupByLibrary.simpleMessage("Персонажи"),
        "russian": MessageLookupByLibrary.simpleMessage("русский"),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "totalPersonages":
            MessageLookupByLibrary.simpleMessage("Всего персонажей"),
        "tryAgain": MessageLookupByLibrary.simpleMessage(
            "Пожалуйста, попробуйте снова"),
        "unknown": MessageLookupByLibrary.simpleMessage("Незвестно")
      };
}
