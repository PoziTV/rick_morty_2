import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rick_morty/constants/app_assets.dart';
import 'package:rick_morty/constants/app_colors.dart';
import 'package:rick_morty/constants/app_styles.dart';

import 'package:rick_morty/screens/heroes_screen/heroes_screen.dart';

import 'package:rick_morty/screens/settings_screen/settings_screen.dart';

import 'package:rick_morty/generated/l10n.dart';

class CommonBottomAppBar extends StatelessWidget {
  final List<bool> activeItem = [
    false,
    false,
  ];

  static const _itemHeroes = 0;
  static const _itemSettings = 1;

  CommonBottomAppBar.heroes({Key? key}) : super(key: key) {
    activeItem[_itemHeroes] = true;
  }

  CommonBottomAppBar.settings({Key? key}) : super(key: key) {
    activeItem[_itemSettings] = true;
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: AppColors.bottomAppBarColor,
      child: Container(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
          top: 8,
          bottom: 12,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: InkWell(
                  child: Column(
                    children: [
                      SvgPicture.asset(
                        AppAssets.svg.bottomAppBarHeroesIcon,
                        color: activeItem[_itemHeroes]
                            ? AppColors.bottomAppBarActiveItemColor
                            : AppColors.bottomAppBarInactiveItemColor,
                        width: 24,
                      ),
                      Text(
                        S.of(context).personages, // 'Персонажи',
                        style: AppStyles.textAppearanceCaption.copyWith(
                          color: activeItem[_itemHeroes]
                              ? AppColors.bottomAppBarActiveItemColor
                              : AppColors.bottomAppBarInactiveItemColor,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                    mainAxisSize: MainAxisSize.min,
                  ),
                  onTap:
                      () => activeItem[_itemSettings]
                          ? {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                  builder: (context) => const HeroesScreen(),
                                ),
                              ),
                            }
                          : null),
            ),
            Expanded(
              child: InkWell(
                  child: Column(
                    children: [
                      SvgPicture.asset(
                        AppAssets.svg.bottomAppBarSettingsIcon,
                        color: activeItem[_itemSettings]
                            ? AppColors.bottomAppBarActiveItemColor
                            : AppColors.bottomAppBarInactiveItemColor,
                        width: 24,
                      ),
                      Text(
                        S.of(context).settings, // 'Настройки',
                        style: AppStyles.textAppearanceCaption.copyWith(
                          color: activeItem[_itemSettings]
                              ? AppColors.bottomAppBarActiveItemColor
                              : AppColors.bottomAppBarInactiveItemColor,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                    mainAxisSize: MainAxisSize.min,
                  ),
                  onTap: () => activeItem[_itemSettings]
                      ? null
                      : {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) => const SettingsScreen(),
                            ),
                          ),
                        }),
            ),
          ],
        ),
      ),
      elevation: 0,
    );
  }
}
