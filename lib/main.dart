import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rick_morty/repository/repo_settings.dart';
import 'package:rick_morty/screens/splash_screen/splash_screen.dart';
import 'generated/l10n.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final repoSetttings = RepoSettings();
  await repoSetttings.init();
  Locale defaultLocale = const Locale('ru', 'RU');
  final locale = await repoSetttings.readLocale();
  if (locale == 'en') {
    defaultLocale = const Locale('en');
  }
  runApp(MyApp(
    locale: defaultLocale,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.locale}) : super(key: key);

  final Locale locale;
  @override
  Widget build(BuildContext context) {
    // Locale _locale = const Locale('ru', 'RU');
    // Locale _locale = const Locale('en');
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: locale,
      supportedLocales: S.delegate.supportedLocales,
      home: const SplashScreen(),
    );
  }
}
