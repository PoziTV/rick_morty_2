import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rick_morty/common_components/common_bottom_app_bar.dart';
import 'package:rick_morty/generated/l10n.dart';
import 'package:rick_morty/repository/repo_settings.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    void changeLocale(String? value) async {
      if (value == null) return;
      if (value == 'en') {
        await S.load(
          const Locale('en'),
        );
        setState(() {});
      } else {
        await S.load(
          const Locale('ru_RU'),
        );
        setState(() {});
      }
      RepoSettings().saveLocale(value);
    }

    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(S.of(context).settings),
            Center(
              child: DropdownButton<String>(
                value: Intl.getCurrentLocale(),
                items: [
                  DropdownMenuItem(
                    value: 'en',
                    child: Text(
                      S.of(context).english,
                    ),
                  ),
                  DropdownMenuItem(
                    value: 'ru_RU',
                    child: Text(
                      S.of(context).russian,
                    ),
                  ),
                ],
                onChanged: changeLocale,
              ),
            ),
          ],
        ),
        bottomNavigationBar: CommonBottomAppBar.settings(),
      ),
    );
  }
}
