
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rick_morty/constants/app_assets.dart';
import 'package:rick_morty/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:rick_morty/constants/app_styles.dart';

class ServiceBar extends StatelessWidget implements PreferredSizeWidget {
  final int heroesTotal;
  final bool isGrid;
   void Function()? changeViewCallBack = () {};

  ServiceBar({Key? key,
    this.heroesTotal = 0,
    this.changeViewCallBack,
    required this.isGrid,
  }) : preferredSize = const Size.fromHeight(68), super(key: key);

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 24),
      padding: const EdgeInsets.only(
        // left: 16,
        right: 12,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${S.of(context).totalPersonages}: $heroesTotal",
            // "ВСЕГО ПЕРСОНАЖЕЙ: $heroesTotal",
            style: AppStyles.textServiceBar,
          ),
          GestureDetector(
            child: SvgPicture.asset(
              isGrid ? AppAssets.svg.listViewIcon : AppAssets.svg.gridViewIcon,
            ),
            onTap: changeViewCallBack,
          ),
        ],
      ),
    );
  }
}
