import 'package:flutter/material.dart';
import 'package:rick_morty/models/hero_model.dart';

import 'heroes_grid_view.dart';
import 'heroes_list_view.dart';

class SelectableHeroesSet extends StatelessWidget {
  final bool isGrid;
  final List<HeroModel> heroesSet;

  const SelectableHeroesSet(
      {Key? key, required this.isGrid, required this.heroesSet}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return isGrid
        ? HeroesGridView(heroesSet: heroesSet)
        : HeroesListView(heroesSet: heroesSet);
  }
}
