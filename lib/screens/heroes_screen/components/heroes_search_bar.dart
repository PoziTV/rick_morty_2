import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rick_morty/constants/app_assets.dart';
import 'package:rick_morty/constants/app_colors.dart';
import 'package:rick_morty/constants/app_styles.dart';

import 'package:rick_morty/generated/l10n.dart';

TextEditingController _textController = TextEditingController();

class HeroesSearchBar extends StatelessWidget {
  const HeroesSearchBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (kDebugMode) {
      print(S.delegate.supportedLocales);
    }
    return Container(
      margin: const EdgeInsets.only(
        top: 11,
      ),
      height: 50,
      child: TextField(
        controller: _textController,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(
            vertical: 12,
          ),
          fillColor: AppColors.gray6,
          filled: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(24),
            borderSide: BorderSide.none,
          ),
          prefixIcon: IconButton(
            icon: SvgPicture.asset(
              AppAssets.svg.searchIcon,
            ),
            onPressed: () {
              if (kDebugMode) {
                print("doSearch button");
              }
            },
          ),
          hintText: S.of(context).findPersonage, //"Найти персонажа",
          hintStyle: AppStyles.textAppearanceBody1,
          suffixIcon: IntrinsicHeight(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                VerticalDivider(
                  indent: 12,
                  endIndent: 12,
                  thickness: 1,
                  color: Colors.white.withOpacity(0.1),
                ),
                IconButton(
                  icon: SvgPicture.asset(
                    AppAssets.svg.filterIcon,
                  ),
                  onPressed: () {
                    if (kDebugMode) {
                      print("filter button");
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
