import 'package:flutter/material.dart';
import 'package:rick_morty/constants/app_styles.dart';
import 'package:rick_morty/constants/app_colors.dart';
import 'package:rick_morty/models/hero_model.dart';
import 'package:rick_morty/constants/app_assets.dart';

import 'package:rick_morty/generated/l10n.dart';

class HeroesGridView extends StatelessWidget {
  final List<HeroModel> heroesSet;
  const HeroesGridView({Key? key, required this.heroesSet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: heroesSet.length,
      itemBuilder: (BuildContext context, int index) => InkWell(
        child: _HeroGridViewCard(currentHero: heroesSet[index]),
        onTap: () {},
      ),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 0.85,
      ),
      // physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
    );
  }
}

class _HeroGridViewCard extends StatelessWidget {
  const _HeroGridViewCard({required this.currentHero});

  final HeroModel currentHero;
  final double _radius = 61;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleAvatar(
          radius: _radius,
          backgroundImage:
              (currentHero.imageName == '' || currentHero.imageName == null)
                  ? AssetImage(AppAssets.images.noAvatar) as ImageProvider
                  : NetworkImage(currentHero.imageName!),
        ),
        // CachedNetworkImage(
        //   imageUrl: currentHero.imageName ?? '',
        //   imageBuilder: (context, imageProvider) =>
        //       CircleAvatar(radius: _radius, backgroundImage: imageProvider),
        //   placeholder: (context, url) => const CircularProgressIndicator(),
        //   errorWidget: (context, url, error) => Image.asset(
        //     AppAssets.images.noAvatar,
        //     width: _radius * 2,
        //     height: _radius * 2,
        //   ),
        // ),
        const Padding(padding: EdgeInsets.only(top: 18)),
        Text(
          currentHero.statusDecoder(),
          style: AppStyles.textAppearanceOverline.copyWith(
            color: currentHero.aliveStatus == "Alive"
                ? AppColors.personAlive
                : AppColors.personDead,
          ),
        ),
        Text(
          currentHero.fullName ?? '',
          style: AppStyles.textPersonName,
        ),
        Text(
          "${currentHero.race ?? S.of(context).unknown}, ${currentHero.gender ?? S.of(context).unknown}",
          // "${currentHero.race}, ${currentHero.gender}",
          style: AppStyles.textAppearanceCaption,
        ),
      ],
    );
  }
}
