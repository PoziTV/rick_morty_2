import 'package:flutter/material.dart';
import 'package:rick_morty/constants/app_styles.dart';
import 'package:rick_morty/constants/app_colors.dart';
import 'package:rick_morty/models/hero_model.dart';
import 'package:rick_morty/constants/app_assets.dart';

import 'package:rick_morty/generated/l10n.dart';

class HeroesListView extends StatelessWidget {
  final List<HeroModel> heroesSet;
  const HeroesListView({Key? key, required this.heroesSet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: heroesSet.length,
      itemBuilder: (BuildContext context, int index) => InkWell(
        child: _HeroListViewCard(currentHero: heroesSet[index]),
        onTap: () {},
      ),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      shrinkWrap: true,
      // physics: NeverScrollableScrollPhysics(),
      separatorBuilder: (BuildContext context, int index) => Container(
        height: 24,
      ),
    );
  }
}

class _HeroListViewCard extends StatelessWidget {
  final HeroModel currentHero;
  final double _radius = 37;

  const _HeroListViewCard({required this.currentHero});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          radius: _radius,
          backgroundImage:
              (currentHero.imageName == '' || currentHero.imageName == null)
                  ? AssetImage(AppAssets.images.noAvatar) as ImageProvider
                  : NetworkImage(currentHero.imageName!),
        ),
        // CachedNetworkImage(
        //   imageUrl: currentHero.imageName ?? '',
        //   imageBuilder: (context, imageProvider) =>
        //       CircleAvatar(radius: _radius, backgroundImage: imageProvider),
        //   placeholder: (context, url) => const CircularProgressIndicator(),
        //   errorWidget: (context, url, error) => Image.asset(
        //     AppAssets.images.noAvatar,
        //     width: _radius * 2,
        //     height: _radius * 2,
        //   ),
        // ),
        Container(
          width: 18,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              currentHero.statusDecoder(),
              style: AppStyles.textAppearanceOverline.copyWith(
                color: currentHero.aliveStatus == "Alive"
                    ? AppColors.personAlive
                    : AppColors.personDead,
              ),
            ),
            Text(
              currentHero.fullName ?? '',
              style: AppStyles.textPersonName,
            ),
            Text(
              "${currentHero.race ?? S.of(context).unknown}, ${currentHero.gender ?? S.of(context).unknown}",
              style: AppStyles.textAppearanceCaption,
            ),
          ],
        ),
      ],
    );
  }
}
