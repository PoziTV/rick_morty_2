import 'package:flutter/material.dart';
import 'package:rick_morty/common_components/common_bottom_app_bar.dart';

import 'package:rick_morty/constants/app_colors.dart';
import 'package:rick_morty/repository/repository.dart';
import 'components/heroes_grid_view.dart';
import 'components/heroes_list_view.dart';
import 'components/heroes_search_bar.dart';
import 'components/serviсe_bar.dart';

class HeroesScreen extends StatefulWidget {
  const HeroesScreen({Key? key}) : super(key: key);

  @override
  State<HeroesScreen> createState() => _HeroScreenState();
}

class _HeroScreenState extends State<HeroesScreen> {
  bool isGrid = false;

  final Repository _repository = Repository();

  @override
  Widget build(BuildContext context) {
    _repository.getHeroesSet();

    return Container(
      decoration: const BoxDecoration(
        color: AppColors.lightThemeScreenBackground,
      ),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black.withOpacity(0),
          appBar: AppBar(
            automaticallyImplyLeading: false,
            elevation: 0,
            backgroundColor: AppColors.lightThemeScreenBackground,
            title: const HeroesSearchBar(), //'Найти персонажа'),
            bottom: ServiceBar(
              heroesTotal: _repository.heroesSet.length,
              changeViewCallBack: () {
                isGrid = !isGrid;
                setState(() {});
              },
              isGrid: isGrid,
            ),
          ),
          body:
          isGrid
              ? HeroesGridView(heroesSet: _repository.heroesSet)
              : HeroesListView(heroesSet: _repository.heroesSet),
          bottomNavigationBar: CommonBottomAppBar.heroes(),
        ),
      ),
    );
  }
}
