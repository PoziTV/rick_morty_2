import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:rick_morty/constants/app_assets.dart';
import 'package:rick_morty/constants/app_colors.dart';
import 'package:rick_morty/generated/l10n.dart';
import 'package:rick_morty/screens/heroes_screen/heroes_screen.dart';

import 'package:rick_morty/auth_data.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  void Function()? onEnterFunction;

  String? login;
  String? pass;

  bool isLoginLenCorrect = false; //false;
  bool isPassLenCorrect = false; //false;

  void onEnterEnableFunction() {
    FocusScope.of(context).unfocus();
    if (AuthData.isAuthCorrect(login, pass)
        ) {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => const HeroesScreen()));
    } else {
      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text(S.of(context).tryAgain), //"Попробуйте снова"),
          actions: [
            TextButton(
              child: Text(S.of(context).close), //"Закрыть"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (kDebugMode) {
      print('start loginscreen');
    }
    final double viewPortHeight = MediaQuery.of(context).size.height;
    const double wordRicNkMorty = 379;
    final double viewPortCoefficient = viewPortHeight / 816;

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(28),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Image.asset(
                AppAssets.images.rickNMortyWords,
                height: wordRicNkMorty * viewPortCoefficient,
              ),
            ),
            // const Spacer(),
            Text(S.of(context).login),
            const Padding(
              padding: EdgeInsets.only(bottom: 8),
            ),
            TextFormField(
              decoration: InputDecoration(
                hintStyle: const TextStyle(fontSize: 12),
                contentPadding: const EdgeInsets.symmetric(
                  vertical: 16.0,
                ),
                prefixIcon: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: SvgPicture.asset(
                    AppAssets.svg.accountIcon,
                    width: 14,
                    height: 16,
                  ),
                ),

                fillColor: AppColors.gray6,
                filled: true,
                // labelText: S.of(context).login, //'Логин',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: BorderSide.none,
                ),
                counterText: '',
              ),
              maxLength: 8,
              maxLines: 1,
              validator: (String? value) {
                return (value != null && value.length < 3 && value.isNotEmpty)
                    ? S
                        .of(context)
                        .loginLenWarning //'Логин должен содержать не менее 8 символов'
                    : null;
              },
              autovalidateMode: AutovalidateMode.always,
              onChanged: (value) {
                isLoginLenCorrect = value.length >= 3;
                setState(() {
                  login = value;
                  onEnterFunction = isLoginLenCorrect && isPassLenCorrect
                      ? onEnterFunction = onEnterEnableFunction
                      : null;
                });
              },
            ),
            const SizedBox(
              height: 10,
            ),
            Text(S.of(context).pass),
            const Padding(
              padding: EdgeInsets.only(bottom: 8),
            ),
            TextFormField(
              autovalidateMode: AutovalidateMode.always,
              decoration: InputDecoration(
                prefixIcon: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: SvgPicture.asset(
                    AppAssets.svg.passIcon,
                    width: 14,
                    height: 16,
                  ),
                ),
                contentPadding: const EdgeInsets.symmetric(
                  vertical: 16.0,
                ),
                fillColor: AppColors.gray6,
                filled: true,
                // labelText: S.of(context).pass, //'Пароль',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  borderSide: BorderSide.none,
                ),
                counterText: '',
              ),
              maxLength: 16,
              maxLines: 1,
              obscureText: true,
              validator: (String? value) {
                return (value != null && value.length < 8 && value.isNotEmpty)
                    ? S
                        .of(context)
                        .passLenWarning //'Логин должен содержать не менее 8 символов'
                    : null;
              },
              onChanged: (value) {
                isPassLenCorrect = value.length >= 8;
                setState(
                  () {
                    pass = value;
                    onEnterFunction = isLoginLenCorrect && isPassLenCorrect
                        ? onEnterFunction = onEnterEnableFunction
                        : null;
                  },
                );
              },
            ),
            const SizedBox(
              height: 24,
            ),
            ElevatedButton(
              onPressed: onEnterFunction,
              child: Text(
                S.of(context).enter,
              ),
              style: ElevatedButton.styleFrom(
                primary: AppColors.primary,
                elevation: 0.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                ),
                padding: const EdgeInsets.symmetric(
                  vertical: 12.0,
                ),
              ),
            ), //"Вход"),
            const SizedBox(
              height: 10,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(S.of(context).noAccYetQuestion),
              const Padding(
                padding: EdgeInsets.only(right: 5),
              ),
              GestureDetector(
                onTap: () {
                  if (kDebugMode) {
                    print("create acc");
                  }
                },
                child: Text(
                  S.of(context).create,
                  style: const TextStyle(
                    color: AppColors.green3,
                  ),
                ),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}
