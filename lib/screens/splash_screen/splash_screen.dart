import 'package:flutter/material.dart';
import 'package:rick_morty/constants/app_assets.dart';
import 'package:rick_morty/constants/app_colors.dart';

import 'package:rick_morty/screens/login_screen/login_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 2), () {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => const LoginScreen(),
        ),
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double viewPortHeight = MediaQuery.of(context).size.height;
    final double viewPortHeight_05 = viewPortHeight * 0.5;
    final double viewPortCoefficient = viewPortHeight / 816;
    const double wordRicNkMorty = 379;
    const double topWordRicNkMorty = 59;
    const double mortyHeight = 205;
    const double rickHeight = 217;

    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: const ColorFilter.mode(
                  AppColors.screenBackGroundDarkTheme, BlendMode.lighten),
              fit: BoxFit.cover,
              image: Image.asset(
                AppAssets.images.starSky,
              ).image,
            ),
          ),
        ),
        Container(
          height: viewPortHeight_05,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.black.withOpacity(0.27),
                Colors.black.withOpacity(0)
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
        ),
        Positioned(
          top: topWordRicNkMorty * viewPortCoefficient,
          child: Image.asset(
            AppAssets.images.rickNMortyWords,
            height: wordRicNkMorty * viewPortCoefficient,
          ),
        ),
        Positioned(
          top: viewPortHeight_05,
          child: Image.asset(
            AppAssets.images.morty,
            height: mortyHeight * viewPortCoefficient,
          ),
        ),
        Positioned(
          bottom: 0,
          child: Image.asset(
            AppAssets.images.rick,
            height: rickHeight * viewPortCoefficient,
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const LoginScreen(),
              ),
            );
          },
        ),
      ],
    );
  }
}
