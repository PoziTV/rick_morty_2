import '../generated/l10n.dart';

class HeroModel {
  HeroModel({
    this.id,
    this.fullName,
    required this.aliveStatus,
    this.about,
    this.gender,
    this.race,
    required this.imageName,
  });

  final int? id;

  final String? fullName;
  final String aliveStatus;
  final String? about;
  final String? gender;
  final String? race;

  final String? imageName;

  String statusDecoder() {
    switch (aliveStatus) {
      case 'Alive':
        return S.current.alive;
      case 'Dead':
        return S.current.dead;
      default:
        return S.current.unknown;
    }
  }
}
