import 'package:rick_morty/models/hero_model.dart';
import 'package:rick_morty/repository/local_data/heroes_set.dart' as local_heroes_set;

class Repository{
  List <HeroModel> heroesSet = [];

  void getHeroesSet(){
     heroesSet = local_heroes_set.heroesSet;
  }
}