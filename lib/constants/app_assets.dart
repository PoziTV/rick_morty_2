abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();
  final String noAvatar = 'assets/images/bitmap/no_avatar.png';
  final String starSky = 'assets/images/bitmap/star_sky_image.png';
  final String backGroundGradient = 'assets/images/bitmap/back_ground_gradient.png';
  final String rick = 'assets/images/bitmap/rick.png';
  final String morty = 'assets/images/bitmap/morty.png';
  final String rickNMortyWords = 'assets/images/bitmap/rick_n_morty_word.png';

}

class _Svg {
  const _Svg();
  final String filterIcon = 'assets/images/svg/filterIcon.svg';
  final String searchIcon = 'assets/images/svg/searchIcon.svg';
  final String gridViewIcon = 'assets/images/svg/gridViewIcon.svg';
  final String listViewIcon = 'assets/images/svg/listViewIcon.svg';
  final String accountIcon = 'assets/images/svg/accountIcon.svg';
  final String passIcon = 'assets/images/svg/passIcon.svg';
  final String bottomAppBarHeroesIcon = 'assets/images/svg/bottomAppBarHeroes.svg';
  final String bottomAppBarSettingsIcon = 'assets/images/svg/bottomAppBarSettings.svg';


}
