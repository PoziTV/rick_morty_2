import 'package:flutter/cupertino.dart';
import 'package:rick_morty/constants/app_colors.dart';

class AppStyles {
  //styleName: textAppearanceBody1;

  // static const textAppearanceBody1 = TextStyle(
  // fontFamily: Roboto,
  //  fontSize: 16,
  //  fontWeight: FontWeight.w400,
  //  // lineheight: 24px,
  //  letterSpacing: 0.4444443881511688,
  //  // textalign: left,
  //  );
  static const TextStyle textAppearanceBody1 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontFamily: ('Roboto'),
    fontStyle: FontStyle.normal,
    height: (24 / 16),
    letterSpacing: 0.44,
    color: AppColors.gray4,
  );

//styleName: textAppearanceOverline;
//     font-family: Roboto;
//     font-size: 10px;
//     font-weight: 500;
//     line-height: 16px;
//     letter-spacing: 1.5px;
//     text-align: left;

  static const TextStyle textAppearanceOverline = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    fontFamily: ('Roboto'),
    fontStyle: FontStyle.normal,
    height: (16 / 10),
    letterSpacing: 1.5,
  );

// font-family: Roboto;
  // font-size: 16px;
  // font-weight: 500;
  // line-height: 24px;
  // letter-spacing: 0.5px;
  // text-align: left;

  static const TextStyle textPersonName = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    fontFamily: ('Roboto'),
    fontStyle: FontStyle.normal,
    height: (24 / 16),
    letterSpacing: 0.5,
    color: AppColors.personName,
  );

  //styleName: textAppearanceCaption;
  // font-family: Roboto;
  // font-size: 12px;
  // font-weight: 400;
  // line-height: 16px;
  // letter-spacing: 0.5px;
  // text-align: left;

  static const TextStyle textAppearanceCaption = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontFamily: ('Roboto'),
    fontStyle: FontStyle.normal,
    height: (16 / 16),
    letterSpacing: 0.5,
    color: AppColors.gray3,
  );

  // font-family: Roboto;
  // font-size: 10px;
  // font-weight: 500;
  // line-height: 16px;
  // letter-spacing: 1.5px;
  // text-align: left;

  static const TextStyle textServiceBar = TextStyle(
      fontSize: 10,
      fontWeight: FontWeight.w500,
      fontFamily: ('Roboto'),
      fontStyle: FontStyle.normal,
      height: (16 / 10),
      letterSpacing: 1.5,
      color: AppColors.gray3);
}

// class themeTextStyles {
//     static const TextStyle textAppearanceBody1 = TextStyle(
//         fontSize: 16,
//         fontWeight: FontWeight.w400,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (24 / 16),
//         letterSpacing: 0.44,
//         color: AppColors.HintText,
//     );
//
//     static const TextStyle textAppearanceBody2 = TextStyle(
//         fontSize: 14,
//         fontWeight: FontWeight.w400,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (20 / 14),
//         letterSpacing: 0.25,
//         color: AppColors.NameWhite,
//     );
//
//
//
//
//
//     static const TextStyle textAppearanceSubtitle1 = TextStyle(
//         fontSize: 16,
//         fontWeight: FontWeight.w400,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (24 / 16),
//         letterSpacing: 0.15,
//         color: AppColors.NameWhite,
//     );
//
//     static const TextStyle textAppearanceSubtitle2 = TextStyle(
//         fontSize: 14,
//         fontWeight: FontWeight.w500,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (20 / 14),
//         letterSpacing: 0.5,
//         color: AppColors.NameWhite,
//     );
//
//
//
//     static const TextStyle textAppearanceHeadline4 = TextStyle(
//         fontSize: 34,
//         fontWeight: FontWeight.w400,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: 1,
//         letterSpacing: 0.25,
//         color: AppColors.NameWhite,
//     );
//
//     static const TextStyle textAppearanceHeadline6 = TextStyle(
//         fontSize: 20,
//         fontWeight: FontWeight.w500,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (28 / 20),
//         letterSpacing: 0.15,
//         color: AppColors.NameWhite,
//     );
//
//     static const TextStyle textAppearanceButton = TextStyle(
//         fontSize: 14,
//         fontWeight: FontWeight.w500,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (24 / 14),
//         letterSpacing: 1.5,
//         color: AppColors.ServiceBarText,
//     );
//
//     static const TextStyle textDescription = TextStyle(
//         fontSize: 13,
//         fontWeight: FontWeight.w400,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (19.5 / 13),
//         letterSpacing: 0.5,
//         color: AppColors.NameWhite,
//     );
//
//     static TextStyle textOverline = TextStyle(
//         fontSize: 10,
//         // fontWeight: FontWeight.w500,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (16 / 10),
//         letterSpacing: 1.5,
//         color: AppColors.OverLine,
//     );
//
//     static TextStyle textEpisodeName = TextStyle(
//         fontSize: 16,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (24 / 16),
//         letterSpacing: 0.5,
//         color: AppColors.NameWhite.withOpacity(0.87),
//     );
//
//     static TextStyle textBody2 = TextStyle(
//         fontSize: 14,
//         fontFamily: ('Roboto'),
//         fontStyle: FontStyle.normal,
//         height: (20 / 14),
//         letterSpacing: 0.25,
//         color: AppColors.Body2,
//     );
//
//     static TextStyle textEpisodProfileName = TextStyle(
//         fontSize: 24,
//         fontFamily: ('Roboto'),
//         fontWeight: FontWeight.w700,
//         fontStyle: FontStyle.normal,
//         height: (32 / 24),
//         // letterSpacing: 0.25,
//         color: AppColors.NameWhite,
//     );
//
//     static TextStyle textSettingsDescription = TextStyle(
//         fontSize: 13,
//         fontFamily: ('Roboto'),
//         fontWeight: FontWeight.w400,
//         fontStyle: FontStyle.normal,
//         height: (20 / 13),
//         letterSpacing: 0.25,
//         color: AppColors.NameWhite,
//     );
// }
