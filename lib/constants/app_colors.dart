import 'dart:ui';

class AppColors {
  static const lightThemeScreenBackground = Color(0xffFCFCFC);//Color(0xffE5E5E5);
  static const gray3 = Color(0xff828282);
  static const gray4 = Color(0xffBDBDBD);
  static const gray6 = Color(0xfff2F2F2);
  static const personAlive = Color(0xff27AE60); //Green2
  static const personDead = Color(0xffEB5757); //red
  static const borders = Color(0xff5B6975);
  static const personName = Color(0xff0B1E2D);
  static const screenBackGroundDarkTheme = Color(0xff0B1E2D);
  static const green3 = Color(0xff43D049);
  static const bottomAppBarColor = Color(0xffffffff);
  static const bottomAppBarActiveItemColor = Color(0xff22A2BD);
  static const bottomAppBarInactiveItemColor = Color(0xffBDBDBD);
  static const primary = Color(0xff22A2BD);

}
